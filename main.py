import PyPDF2
import os
import urllib.request
import requests


path = "C:/Users/ofek/PycharmProjects/BookScrubber/AllBooks"
baseurl = "https://link.springer.com/content/pdf/"

pdfFileObj = open('Springer Ebooks.pdf', 'rb')

pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

# printing number of pages in pdf file
print(pdfReader.numPages)

# creating a page object
count = 0
for page in range(21):
    pageObj = pdfReader.getPage(page)

    # extracting text from page
    text = pageObj.extractText()
    postString = text.split("\n")
    if page == 0:
        postString = postString[5:]
    bookName = postString[1]
    bookName = bookName.replace(":","").replace("?","").replace("*","").replace("/", "")
    for line in range(len(postString)):
        if postString[line].find("http://") != -1:
            bookLink = postString[line]
            print(bookName)
            try:
                os.mkdir(path + "/" + bookName)
            except:
                print("Creation of the directory %s failed" % bookName)
            print(bookLink)
            r = requests.get(bookLink)
            bookid = r.url[r.url.find("/book/") + 6:]
            print('Beginning file download...')
            if not os.path.isfile(path + "/" + bookName + "/" + bookName.replace(" ", "") + ".pdf") :
                urllib.request.urlretrieve(baseurl + "/" + bookid + ".pdf",path + "/" + bookName.rstrip() + "/" + bookName.replace(" ", "") + ".pdf")
            else:
                print("File already exists, skipping")
            count = count + 1
            try :
                bookName = postString[line + 2]
                bookName = bookName.replace(":", "").replace("?", "").replace("*", "").replace("/", "")
            except :
                break

print(count)
# closing the pdf file object
pdfFileObj.close()